
class Conversor(object):

    _conversiones = {
        'euro': {
            "sol": 1.1,
            "real": 1.2,
            "peso uruguayo": 1.3,
            "peso chileno": 1.4
        },
        'sol': {
            "euro": 2.1,
            "real": 2.2,
            "peso uruguayo": 2.3,
            "peso chileno": 2.4
        },
        'real': {
            "euro": 3.1,
            "sol": 3.2,
            "peso uruguayo": 3.3,
            "peso chileno": 3.4
        },
        'peso uruguayo': {
            "euro": 4.1,
            "real": 4.2,
            "sol": 4.3,
            "peso chileno": 4.4
        },
        'peso chileno': {
            "euro": 5.1,
            "real": 5.2,
            "sol": 5.3,
            "peso uruguayo": 5.4
        }
    }

    def __init__(self, moneda, tipo_cambio, cantidad):
        self.moneda = moneda
        self.tipo_cambio = tipo_cambio
        self.cantidad = cantidad

    @classmethod
    def conversiones_disponibles(cls, moneda=None):
        if moneda:
            try:
                return cls._conversiones[str(moneda).lower()].keys()
            except KeyError:
                print("Moneda \"%s\" no disponible." % (moneda,))
                raise
        else:
            return cls._conversiones.keys()

    def convertir(self):
        try:
            self.conversion = self._conversiones.\
                                get(self.moneda).get(self.tipo_cambio) * self.cantidad
        except TypeError:
            print("Datos incorrectos incorrectos o faltantes convertir.")
            raise


def menu(moneda=None):
    conversiones_disponibles = Conversor.conversiones_disponibles(moneda)
    print('\n'.join(map(lambda x: "* " + x, conversiones_disponibles)))


def main():
    while True:
        print('Elige una moneda a convertir disponible: ')
        menu()
        moneda = str(input('> '))
        
        print('Elige un tipo de cambio disponible: ')
        menu(moneda)
        tipo_cambio = str(input('> '))

        print('Elige la cantidad a convertir: ')
        cantidad = int(input('> '))

        conversor1 = Conversor(moneda, tipo_cambio, cantidad)
        conversor1.convertir()

        print("La conversion de {} unidades de {} a {} es:".format(conversor1.cantidad,
                                                        conversor1.moneda,
                                                        conversor1.tipo_cambio))
        print(conversor1.conversion)
    
if __name__ == '__main__':
    main()
